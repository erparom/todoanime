<?php

namespace TodoAnime\PortalBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('TodoAnimePortalBundle:Default:index.html.twig', array('name' => $name));
    }
}
